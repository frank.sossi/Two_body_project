# Two_body_project
Project implementing CUDA operations for Orbital mechanics

@startuml

class Body {
  vector<float> position
  vector<float> velocity
  float mass

  +__device__ void initialize(vector<float> pos, vector<float> vel, float m)
  +__device__ void computeForce(Body otherBody)
  +__device__ void updatePosition(float dt)
  +__device__ void updateVelocity(float dt)
}

class TwoBodyProblem {
  Body body1
  Body body2
  float dt

  +__device__ void initialize(vector<float> pos1, vector<float> vel1, float m1, vector<float> pos2, vector<float> vel2, float m2, float timeStep)
  +__global__ void simulate()
}

Body --> TwoBodyProblem

@enduml


![PlantUML model](https://www.plantuml.com/plantuml/png/FOsx2i90401xlq8BIxDYQPNAMj0-NEuMitAtU-o3uD-h25R3mC2CvaazMjtiY7EDWg5rkxXtEmDeMeS7-H9p66eMTcGViZIu76vYHd1VPPgblAQkyiJiq18nN7-oFnqbl1BZlnlwzpKYbm_V)

![PlantUML model](https://www.plantuml.com/plantuml/png/ZP5HIWCn48RVSugXJnNP2nIH2gAlOht0a1stmMIpp4mMYlRkreIcn0hhKy8t_-IV-RCLHTOKl9dGVk10iDgva3ogY-CAFWs0zIXomgZalLCg5E1sYk9-P1kOoMhaGcVoqJezjFGziYNP03BZeI0RmvIKdd9bVLEW6vK6HgKCZkRYdtN5kpPVgvuPPwTvUlJ_QtdG46NQ4plxo3WiwdVH8xzYxxBB2vD8ucMRjpqdqKLPctwMSXhFyM3VrtzOo_csWGhhSvL0rzr3Ji90EU7kg-lWEgcs_YkMjlXt1gscB7nYbvNoB7KoSpHR6liK_3S0)
